import Vue from 'vue'
import Vuex from 'vuex'
import api from '../API'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categories: [],
    articles: [],
    token: "",
    userEmail: "",
    tags: [],
  },
  mutations: {
    setCategories(state, payload) {
      state.categories = payload
    },
    setTags(state, payload) {
      state.tags = payload
    },
    setUser(state, payload) {
      state.token = payload.token
      state.userEmail = payload.email
      localStorage.token = payload.token;
      localStorage.user = payload.email;
    },
  },
  actions: {
    loadCategories(context) {
      api.categories.getAll()
        .then((response) => context.commit('setCategories', response.data))
        .catch((error) => alert(error))
    },
    loadTags(context) {
      api.tags.getAll()
        .then((response) => context.commit('setTags', response.data))
        .catch((error) => alert(error))
    },
  },
  modules: {
  },
  getters: {
    isToken: state => !!state.token

  }
})

