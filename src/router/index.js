import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);



const routes = [
  {
    path: "/",
    name: "Home",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Home.vue"),
  },
  {
    path: "/login",
    name: "Login",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Login.vue"),
  },
  {
    path: "/products",
    name: "Perfil",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Perfil.vue"),
  },
  {
    path: "/usuario",
    name: "Usuario",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Usuario.vue"),
  },
  {
    path: "/register",
    name: "Register",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Register.vue"),
  },
  {
    path: "/product-details/category=:category_id/:id",
    name: "ProductDetails",
    props: true,
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ProductDetails.vue"),
  },
  {
    path: "/upload",
    name: "UploadArticle",
    
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/UploadProduct.vue"),
  },
]
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});



export default router;
