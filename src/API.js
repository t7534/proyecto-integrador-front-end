import axios from 'axios';
require('dotenv').config()
const baseURL = 'http://batoipop-back.es/api';

const articles = {
    getAll: (page, filter) => axios.get(`${baseURL}/article?page=${page}&${filter}`),
    getOne: (id) => axios.get(`${baseURL}/article/${id}`),
    create: (item) => axios.post(`${baseURL}/article`, item),
    modify: (item) => axios.put(`${baseURL}/article/${item.article_id}`, item),
    delete: (id) => axios.delete(`${baseURL}/article/${id}`),
};
const categories = {
    getAll: () => axios.get(`${baseURL}/category`),
    getOne: (id) => axios.get(`${baseURL}/category/${id}`),
};
const messages = {
    getAll: (id) => axios.get(`${baseURL}/message/${id}`),
    create: (item) => axios.post(`${baseURL}/message`, item),
};
const users = {
    getOne: (email) => axios.get(`${baseURL}/user/${email}`),
    getArticles: (email) => axios.get(`${baseURL}/userArticles/${email}`),
    login: (item) => axios.post(`${baseURL}/login`, item),
    create: (item) => axios.post(`${baseURL}/user`, item)
};
const tags = {
    getAll: () => axios.get(`${baseURL}/tag`),
}
const reports = {
    createArticle: (item) => axios.post(`${baseURL}/reportArticle`, item),
    createComment: (item) => axios.post(`${baseURL}/reportMessage`, item)
};

export default {
    articles,
    categories,
    messages,
    users,
    tags,
    reports
};
