import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as VueGoogleMaps from "vue2-google-maps";
import VueSimpleAlert from "vue-simple-alert";
 
Vue.use(VueSimpleAlert);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBp3i2pFMN5GxRpuIqqkw5HeFChLHVWzxU",
    libraries: "places"
  },
});


Vue.config.productionTip = false
import "bootstrap/dist/css/bootstrap.min.css";
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
